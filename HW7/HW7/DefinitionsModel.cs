﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HW7
{
    public class DefinitionsModel
    {
        public string Type { get; set; }
        public string Definition { get; set; }
        public string Example { get; set; }
    }
}
