﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace HW7
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void Handle_GetDefinitions(object sender, EventArgs e)
        {
            ErrorLabel.IsVisible = false;

            if(!CrossConnectivity.Current.IsConnected)
            {
                await DisplayAlert("No Internet","No Internet Connection Detected", "OK");
                return;
            }
            string lower = Entry.Text.ToLower();

            string owlBotApiAddress = "https://owlbot.info/api/v2/dictionary/" + lower;
            var Uri = new Uri(owlBotApiAddress);

            var client = new HttpClient();
            var data = new ObservableCollection<DefinitionsModel>();

            HttpResponseMessage response = await client.GetAsync(Uri);
            if(!response.IsSuccessStatusCode)
            {
                ErrorLabel.IsVisible = true;
            }
            else
            {
                string jsonContent = await response.Content.ReadAsStringAsync();
                if(jsonContent == "[]")
                {
                    ErrorLabel.IsVisible = true;
                }
                else
                {
                data = JsonConvert.DeserializeObject<ObservableCollection<DefinitionsModel>>(jsonContent);
                }
            }
            DefinitionsListView.ItemsSource = data;
        }
    }
}
